`ifndef ETH_UDP_SVH
`define ETH_UDP_SVH
class eth_udp;
    virtual mac_bfm bfm;
    /*
     * 静态属性
     */
    udp_pck_s   udp_pck;
    function new(virtual mac_bfm b);
        bfm = b;
    endfunction

    function void udp_config (bit [31:00] command, byte value[]);
        byte stream[];
        bit [31:0] checksum = '0;
        //bit [31:0] old_crc = 32'hff_ff_ff_ff;
        bit [31:0] old_crc = 32'h00_00_00_00;
        int data_len = $size(this.udp_pck.udp.data);

        this.udp_pck.head.preamble = 64'h55_55_55_55_55_55_55_d5;
        this.udp_pck.head.dst_mac_addr = DEV_MAC_ADDR;
        this.udp_pck.head.src_mac_addr = PC_MAC_ADDR;
        this.udp_pck.head.ether_type = IPV4;

        this.udp_pck.ip.version = 4'd4;
        this.udp_pck.ip.ihl = 4'd5;
        this.udp_pck.ip.tos = 8'h00;
        this.udp_pck.ip.tol_length = 16'd20+8+$size(this.udp_pck.udp.data);  // IP首部20字节 + UDP（或者其他子协议）全部数据(8字节首部+数据）
        this.udp_pck.ip.id = 16'h0000;
        this.udp_pck.ip.flags = 3'b010;
        this.udp_pck.ip.frag_offset = 13'd0;
        this.udp_pck.ip.ttl = 8'h40;
        this.udp_pck.ip.protocal = 8'h11;
        this.udp_pck.ip.checksum = 16'hb5ff;
        this.udp_pck.ip.src_ip_addr = PC_IP_ADDR;
        this.udp_pck.ip.dst_ip_addr = DEV_IP_ADDR;

        this.udp_pck.udp.src_port = PC_UDP_PORT;
        this.udp_pck.udp.dst_port = DEV_UDP_PORT;
        this.udp_pck.udp.length   = $size(this.udp_pck.udp.data);
        this.udp_pck.udp.checksum = 16'hdddd;

        for(int i = 0; i < $size(this.udp_pck.udp.data); i = i + 1 )
        begin
            //this.udp_pck.udp.data[i] = $urandom_range(0, 255);
            this.udp_pck.udp.data[i] = 0;
        end
        // 一个udp数据区固定280Byte
        // 帧头
        this.udp_pck.udp.data[0] = 8'haa;
        this.udp_pck.udp.data[1] = 8'h55;
        this.udp_pck.udp.data[2] = 8'h5a;
        this.udp_pck.udp.data[3] = 8'ha5;
        this.udp_pck.udp.data[4] = 8'haa;
        this.udp_pck.udp.data[5] = 8'h55;
        this.udp_pck.udp.data[6] = 8'h5a;
        this.udp_pck.udp.data[7] = 8'ha5;

        // 指令序号
        this.udp_pck.udp.data[08] = 8'h01;
        this.udp_pck.udp.data[09] = 8'h02;
        this.udp_pck.udp.data[10] = 8'h03;
        this.udp_pck.udp.data[11] = 8'h04;

        // 指令
        this.udp_pck.udp.data[12] = command[31:24];
        this.udp_pck.udp.data[13] = command[23:16];
        this.udp_pck.udp.data[14] = command[15:08];
        this.udp_pck.udp.data[15] = command[07:00];

        // 包序号
        this.udp_pck.udp.data[16] = 8'h04;
        this.udp_pck.udp.data[17] = 8'h02;
        this.udp_pck.udp.data[18] = 8'h03;
        this.udp_pck.udp.data[19] = 8'h01;

        // 有效数据长度
        this.udp_pck.udp.data[20] = 8'h00;
        this.udp_pck.udp.data[21] = 8'h00;
        this.udp_pck.udp.data[22] = 8'h00;
        this.udp_pck.udp.data[23] = $size(value);

        // 数据
        for(int i = 0; i < $size(value); i = i + 1 )
        begin
            this.udp_pck.udp.data[i+24] = value[i];
        end

        // 校验和
        for(int i = 8; i < $size(udp_pck.udp.data)-4; i = i + 4 )
        begin
            checksum = {>>{this.udp_pck.udp.data[i+0], this.udp_pck.udp.data[i+1], this.udp_pck.udp.data[i+2], this.udp_pck.udp.data[i+3]}} + checksum;
        end
        this.udp_pck.udp.data[data_len-4] = checksum[31:24];
        this.udp_pck.udp.data[data_len-3] = checksum[23:16];
        this.udp_pck.udp.data[data_len-2] = checksum[15:08];
        this.udp_pck.udp.data[data_len-1] = checksum[07:00];

        stream = {>>{this.udp_pck}};
        // 去掉前导符和最后的4个校验字节
        for(int i = 8; i < $size(stream)-4; i = i + 1 )
        begin
            old_crc = calc_crc(stream[i], old_crc);
        end
        this.udp_pck.checksum = {old_crc[7:0], old_crc[15:8], old_crc[23:16], old_crc[31:24]};
    endfunction: udp_config

    function bit[32:0] calc_crc(bit [7:0] data, bit [31:0] init);
        reg [31:0] crc;
        reg        crc_feedback;
        integer    I;
        begin
            crc = ~ init;
            for (I = 0; I < 8; I = I + 1)
            begin
                crc_feedback = crc[0] ^ data[I];

                crc[0]       = crc[1];
                crc[1]       = crc[2];
                crc[2]       = crc[3];
                crc[3]       = crc[4];
                crc[4]       = crc[5];
                crc[5]       = crc[6]  ^ crc_feedback;
                crc[6]       = crc[7];
                crc[7]       = crc[8];
                crc[8]       = crc[9]  ^ crc_feedback;
                crc[9]       = crc[10] ^ crc_feedback;
                crc[10]      = crc[11];
                crc[11]      = crc[12];
                crc[12]      = crc[13];
                crc[13]      = crc[14];
                crc[14]      = crc[15];
                crc[15]      = crc[16] ^ crc_feedback;
                crc[16]      = crc[17];
                crc[17]      = crc[18];
                crc[18]      = crc[19];
                crc[19]      = crc[20] ^ crc_feedback;
                crc[20]      = crc[21] ^ crc_feedback;
                crc[21]      = crc[22] ^ crc_feedback;
                crc[22]      = crc[23];
                crc[23]      = crc[24] ^ crc_feedback;
                crc[24]      = crc[25] ^ crc_feedback;
                crc[25]      = crc[26];
                crc[26]      = crc[27] ^ crc_feedback;
                crc[27]      = crc[28] ^ crc_feedback;
                crc[28]      = crc[29];
                crc[29]      = crc[30] ^ crc_feedback;
                crc[30]      = crc[31] ^ crc_feedback;
                crc[31]      =           crc_feedback;
            end
            return ~ crc;
        end
    endfunction

    task send_udp(bit [31:00] command, byte value[]);
        byte stream[];
        udp_config(command, value);
        stream = {>>{this.udp_pck}};
        $display("udp frame length is %d", $size(stream));
        bfm.gmii_rx_dv = 0;
        bfm.gmii_rx_er = 0;
        bfm.gmii_rxd = 0;
        for(int i = 0; i < $size(stream); i = i + 1 )
        begin
            @ bfm.ether_rx_cb;
            bfm.gmii_rx_dv = 1;
            bfm.gmii_rxd = stream[i];
        end
        @ bfm.ether_rx_cb;
        bfm.gmii_rx_dv = 0;
    endtask:send_udp
endclass

`endif
