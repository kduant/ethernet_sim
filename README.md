# ethernet_sim
提供arp，ping，udp任务封装


# 用法
```verilog
`include "./ethernet_sim/mac_bfm.svh"
`include "./ethernet_sim/eth_arp.svh"
`include "./ethernet_sim/eth_ping.svh"
`include "./ethernet_sim/eth_udp.svh"

mac_bfm mac_bfm_i();
eth_arp eth_arp_h;
eth_ping eth_ping_h;
eth_udp eth_udp_h;

initial
begin
    eth_arp_h = new(mac_bfm_i);
    eth_ping_h = new(mac_bfm_i);
    eth_udp_h = new(mac_bfm_i);
    #5us eth_arp_h.send_arp();
    #3us eth_ping_h.send_ping();
end

enthernet_top enthernet_topEx01
(
    .phy_resetn      (  mac_bfm_i.phy_resetn          ),
    .gmii_tx_clk     (  mac_bfm_i.gmii_tx_clk         ),
    .gmii_txd        (  mac_bfm_i.gmii_txd            ),
    .gmii_tx_en      (  mac_bfm_i.gmii_tx_en          ),
    .gmii_tx_er      (  mac_bfm_i.gmii_tx_er          ),
    .gmii_rx_clk     (  mac_bfm_i.gmii_rx_clk         ),
    .gmii_rxd        (  mac_bfm_i.gmii_rxd            ),
    .gmii_rx_dv      (  mac_bfm_i.gmii_rx_dv          ),
    .gmii_rx_er      (  mac_bfm_i.gmii_rx_er          )
);
```
